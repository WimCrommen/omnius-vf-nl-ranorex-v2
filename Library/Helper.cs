﻿/*
 * Created by Ranorex
 * User: wim.crommen
 * Date: 17-8-2016
 * Time: 1:03 
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Threading;
using WinForms = System.Windows.Forms;

using Ranorex;
using Ranorex.Core;
using Ranorex.Core.Testing;

namespace omnius_vf_nl_ranorex_v2.Library
{
    /// <summary>
    /// Description of Helper.
    /// </summary>
    [TestModule("F5014CFD-948F-4004-AE58-36DD03609D13", ModuleType.UserCode, 1)]
    public class Helper : ITestModule
    {
        /// <summary>
        /// Constructs a new instance.
        /// </summary>
        public Helper()
        {
            // Do not delete - a parameterless constructor is required!
        }

        /// <summary>
        /// Performs the playback of actions in this module.
        /// </summary>
        /// <remarks>You should not call this method directly, instead pass the module
        /// instance to the <see cref="TestModuleRunner.Run(ITestModule)"/> method
        /// that will in turn invoke this method.</remarks>
        void ITestModule.Run()
        {
            Mouse.DefaultMoveTime = 300;
            Keyboard.DefaultKeyPressTime = 100;
            Delay.SpeedFactor = 1.0;
        }
        
        //check if a string contains multiple values separated by ';' or '\n'
        //E.g. used for adding multiple addons to a single package
        public int checkifmultivalue(string multi, char splitter)
        {
        	int count = 0;
        	if (multi != "")
        	{ 
        		string[] str=multi.Split(splitter);
        		count = str.Length;
        	}
        	return count;
        }
        

        
        public void selectItemAmountfromExcel
        	(string var_list, string tc_single, string tc_multi, char splitter)
        {
        	int cnt = checkifmultivalue(var_list, splitter);
        	if (cnt>1)
        	//multiple items
        	{
        		TestSuite.Current.GetTestCase(tc_single).Checked = false;
        		TestSuite.Current.GetTestCase(tc_multi).Checked = true;
        	}
        	else if (cnt==1)
        	//single item
        	{
        		TestSuite.Current.GetTestCase(tc_single).Checked = true;
        		TestSuite.Current.GetTestCase(tc_multi).Checked = false;
        	}
        	else{
        		TestSuite.Current.GetTestCase(tc_single).Checked = false;
        		TestSuite.Current.GetTestCase(tc_multi).Checked = false;
        	}
        }
        
        public string replacePath(string path, string pattern, string subst)
        {
        	Match matched = Regex.Match(path,pattern);
			string submatched = matched.Groups[1].ToString();
			string result = path.Replace(submatched, subst);
			
			return result;
        }
    }
}
