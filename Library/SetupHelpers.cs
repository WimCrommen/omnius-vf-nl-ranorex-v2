﻿/*
 * Created by Ranorex
 * User: wim.crommen
 * Date: 17-8-2016
 * Time: 1:53 
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Threading;
using WinForms = System.Windows.Forms;

using Ranorex;
using Ranorex.Core;
using Ranorex.Core.Testing;

namespace omnius_vf_nl_ranorex_v2.Library
{
    /// <summary>
    /// Description of SetupHelpers.
    /// </summary>
    [TestModule("D0F2129E-EA98-48E4-BFE0-AA8312377C3B", ModuleType.UserCode, 1)]
    public class SetupHelpers : ITestModule
    {
        public static string args = null;
		public static string browser = null;
		
		private omnius_vf_nl_ranorex_v2Repository repo = omnius_vf_nl_ranorex_v2Repository.Instance;
		
		/// <summary>
        /// Constructs a new instance.
        /// </summary>
        public SetupHelpers()
        {
            // Do not delete - a parameterless constructor is required!
        }

        /// <summary>
        /// Performs the playback of actions in this module.
        /// </summary>
        /// <remarks>You should not call this method directly, instead pass the module
        /// instance to the <see cref="TestModuleRunner.Run(ITestModule)"/> method
        /// that will in turn invoke this method.</remarks>
        void ITestModule.Run()
        {
            Mouse.DefaultMoveTime = 300;
            Keyboard.DefaultKeyPressTime = 100;
            Delay.SpeedFactor = 1.0;
        }
        
        public void OpenBrowser(string arg_testEnvironment, string arg_channel)
        {
			browser = "Chrome"; //Override for browser
			args = "--ignore-certificate-errors --test-type";
        	
        	Host.Local.KillBrowser(browser);
			Delay.Duration(3000, false);
            Report.Log(ReportLevel.Info, "Website", "Opening website in : " + browser + ".");
            Host.Local.OpenBrowser("http://" + getURL(arg_testEnvironment, arg_channel) + "/agent/account/logout", browser, args, false, true, true, true, true);
            Report.Log(ReportLevel.Info, "Website", "Opening website: http://" + getURL(arg_testEnvironment,arg_channel) + ".");
            Delay.Duration(2000, false);
        }

        public void setEnvironment(string var_testEnvironment, string var_channel)
        {
        	Report.Log(ReportLevel.Info, "Env", "LocalTestEnvironment is set to : " + var_channel + " " + var_testEnvironment +".");
        	var testEnvironment = var_testEnvironment.ToLower();
        	var Channel = var_channel.ToLower();
        	string str=null;
        	
        	//changing domain of repo to reflect the correct xpaths
        	switch(Channel.ToLower())
        	{
        		case "telesales":
        			repo.Generic.BasePath="/dom[@domain='"+getURL(testEnvironment, "telesales")+"']";
        			str= repo.Generic.BasePath.ToString();
        			Report.Log(ReportLevel.Info, "Env", "Changing repo.Generic.Basepath to: " + str +" .");
        			repo.Telesales.BasePath="/dom[@domain='"+getURL(testEnvironment, "telesales")+"']";
        			str= repo.Telesales.BasePath.ToString();
        			Report.Log(ReportLevel.Info, "Env", "Changing repo.Telesales.Basepath to: " + str +" .");
        			break;
        		case "indirect":
        			repo.Generic.BasePath="/dom[@domain='"+getURL(testEnvironment, "indirect")+"']";
        			str= repo.Generic.BasePath.ToString();
        			Report.Log(ReportLevel.Info, "Env", "Changing repo.Generic.Basepath to: " + str +" .");
        			break;
        		default:
        			break;
        	}
        	
        	
        }
        
        private string getURL(string testEnvironment, string Channel)
        {
        	string str = "";
        	switch (testEnvironment.ToLower())
        	{
        		case "uat":
        			switch (Channel.ToLower())
        			{
        				case "telesales":
        					str="telesales-ssfe.uat.dynalean.eu";
        					break;
        				case "indirect":
        					str="indirect-ssfe.uat.dynalean.eu";
        					break;
        			}
    				break;
    			case "amdocs":
    				switch (Channel.ToLower())
        			{
        				case "telesales":
        					str="telesales-ssfe.amdocs.dynalean.eu";
        					break;
        				case "indirect":
        					str="indirect-ssfe.amdocs.dynalean.eu";
        					break;
        				case "retail":
        					str="retail-ssfe.amdocs.dynalean.eu";
        					break;
        			}
    				break;
    			default:
    				str="default";
    				break;
        	}
        	return str;
        }
        
        public void Killbrowser()
        {
        	Host.Local.KillBrowser(browser);
        	Report.Log(ReportLevel.Info, "Website", "Killing the currently open browser: " + browser + ".");
        }
    }
}
