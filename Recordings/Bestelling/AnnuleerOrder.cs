﻿///////////////////////////////////////////////////////////////////////////////
//
// This file was automatically generated by RANOREX.
// DO NOT MODIFY THIS FILE! It is regenerated by the designer.
// All your modifications will be lost!
// http://www.ranorex.com
//
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Threading;
using WinForms = System.Windows.Forms;

using Ranorex;
using Ranorex.Core;
using Ranorex.Core.Testing;
using Ranorex.Core.Repository;

namespace omnius_vf_nl_ranorex_v2.Recordings.Bestelling
{
#pragma warning disable 0436 //(CS0436) The type 'type' in 'assembly' conflicts with the imported type 'type2' in 'assembly'. Using the type defined in 'assembly'.
	/// <summary>
	///The AnnuleerOrder recording.
	/// </summary>
	[TestModule("f2cf784b-cb3e-4b79-9ef9-b1050ac96609", ModuleType.Recording, 1)]
	public partial class AnnuleerOrder : ITestModule
	{
		/// <summary>
		/// Holds an instance of the omnius_vf_nl_ranorex_v2.omnius_vf_nl_ranorex_v2Repository repository.
		/// </summary>
		public static omnius_vf_nl_ranorex_v2.omnius_vf_nl_ranorex_v2Repository repo = omnius_vf_nl_ranorex_v2.omnius_vf_nl_ranorex_v2Repository.Instance;

		static AnnuleerOrder instance = new AnnuleerOrder();

		/// <summary>
		/// Constructs a new instance.
		/// </summary>
		public AnnuleerOrder()
		{
		}

		/// <summary>
		/// Gets a static instance of this recording.
		/// </summary>
		public static AnnuleerOrder Instance
		{
			get { return instance; }
		}

#region Variables

#endregion

		/// <summary>
		/// Starts the replay of the static recording <see cref="Instance"/>.
		/// </summary>
		[System.CodeDom.Compiler.GeneratedCode("Ranorex", "6.0")]
		public static void Start()
		{
			TestModuleRunner.Run(Instance);
		}

		/// <summary>
		/// Performs the playback of actions in this recording.
		/// </summary>
		/// <remarks>You should not call this method directly, instead pass the module
		/// instance to the <see cref="TestModuleRunner.Run(ITestModule)"/> method
		/// that will in turn invoke this method.</remarks>
		[System.CodeDom.Compiler.GeneratedCode("Ranorex", "6.0")]
		void ITestModule.Run()
		{
			Mouse.DefaultMoveTime = 300;
			Keyboard.DefaultKeyPressTime = 100;
			Delay.SpeedFactor = 1.00;

			Init();

			Report.Log(ReportLevel.Info, "Wait", "Waiting 5s for item 'Generic.CustomerOrders' to exist.", repo.Generic.CustomerOrdersInfo, new ActionTimeout(5000), new RecordItemIndex(0));
			repo.Generic.CustomerOrdersInfo.WaitForExists(5000);
			
			Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'Generic.BestellingenKlaarVoorUitlevering' at Center.", repo.Generic.BestellingenKlaarVoorUitleveringInfo, new RecordItemIndex(1));
			repo.Generic.BestellingenKlaarVoorUitlevering.Click();
			Delay.Milliseconds(200);
			
			Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'Generic.ButtonTagInzien' at Center.", repo.Generic.ButtonTagInzienInfo, new RecordItemIndex(2));
			repo.Generic.ButtonTagInzien.Click();
			Delay.Milliseconds(200);
			
			Report.Log(ReportLevel.Info, "Wait", "Waiting 10s for item 'Generic.MainLockBtn' to exist.", repo.Generic.MainLockBtnInfo, new ActionTimeout(10000), new RecordItemIndex(3));
			repo.Generic.MainLockBtnInfo.WaitForExists(10000);
			
			Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'Generic.MainLockBtn' at Center.", repo.Generic.MainLockBtnInfo, new RecordItemIndex(4));
			repo.Generic.MainLockBtn.Click();
			Delay.Milliseconds(200);
			
			Report.Log(ReportLevel.Info, "Wait", "Waiting 10s for item 'Generic.ButtonEdit' to exist.", repo.Generic.ButtonEditInfo, new ActionTimeout(10000), new RecordItemIndex(5));
			repo.Generic.ButtonEditInfo.WaitForExists(10000);
			
			Report.Log(ReportLevel.Info, "Invoke Action", "Invoking Focus() on item 'Generic.ButtonEdit'.", repo.Generic.ButtonEditInfo, new RecordItemIndex(6));
			repo.Generic.ButtonEdit.Focus();
			Delay.Milliseconds(0);
			
			Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'Generic.ButtonEdit' at Center.", repo.Generic.ButtonEditInfo, new RecordItemIndex(7));
			repo.Generic.ButtonEdit.Click();
			Delay.Milliseconds(200);
			
			Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'Generic.AnnuleerPakket' at Center.", repo.Generic.AnnuleerPakketInfo, new RecordItemIndex(8));
			repo.Generic.AnnuleerPakket.Click();
			Delay.Milliseconds(200);
			
			Report.Log(ReportLevel.Info, "Wait", "Waiting 10s for item 'Generic.BevestigWijzigingen' to exist.", repo.Generic.BevestigWijzigingenInfo, new ActionTimeout(10000), new RecordItemIndex(9));
			repo.Generic.BevestigWijzigingenInfo.WaitForExists(10000);
			
			Report.Log(ReportLevel.Info, "Invoke Action", "Invoking Focus() on item 'Generic.BevestigWijzigingen'.", repo.Generic.BevestigWijzigingenInfo, new RecordItemIndex(10));
			repo.Generic.BevestigWijzigingen.Focus();
			Delay.Milliseconds(0);
			
			Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'Generic.BevestigWijzigingen' at Center.", repo.Generic.BevestigWijzigingenInfo, new RecordItemIndex(11));
			repo.Generic.BevestigWijzigingen.Click();
			Delay.Milliseconds(200);
			
			Report.Log(ReportLevel.Info, "Wait", "Waiting 10s for item 'Generic.BevestigAnnulering' to exist.", repo.Generic.BevestigAnnuleringInfo, new ActionTimeout(10000), new RecordItemIndex(12));
			repo.Generic.BevestigAnnuleringInfo.WaitForExists(10000);
			
			Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'Generic.BevestigAnnulering' at Center.", repo.Generic.BevestigAnnuleringInfo, new RecordItemIndex(13));
			repo.Generic.BevestigAnnulering.Click();
			Delay.Milliseconds(200);
			
			Report.Log(ReportLevel.Info, "Wait", "Waiting 30s for item 'Generic.GaDoorNaarHomepagina' to exist.", repo.Generic.GaDoorNaarHomepaginaInfo, new ActionTimeout(30000), new RecordItemIndex(14));
			repo.Generic.GaDoorNaarHomepaginaInfo.WaitForExists(30000);
			
			Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'Generic.GaDoorNaarHomepagina' at Center.", repo.Generic.GaDoorNaarHomepaginaInfo, new RecordItemIndex(15));
			repo.Generic.GaDoorNaarHomepagina.Click();
			Delay.Milliseconds(200);
			
		}

#region Image Feature Data
#endregion
	}
#pragma warning restore 0436
}
