﻿///////////////////////////////////////////////////////////////////////////////
//
// This file was automatically generated by RANOREX.
// DO NOT MODIFY THIS FILE! It is regenerated by the designer.
// All your modifications will be lost!
// http://www.ranorex.com
//
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Threading;
using WinForms = System.Windows.Forms;

using Ranorex;
using Ranorex.Core;
using Ranorex.Core.Testing;
using Ranorex.Core.Repository;

namespace omnius_vf_nl_ranorex_v2.Recordings.Multi
{
#pragma warning disable 0436 //(CS0436) The type 'type' in 'assembly' conflicts with the imported type 'type2' in 'assembly'. Using the type defined in 'assembly'.
	/// <summary>
	///The multiCSV_package recording.
	/// </summary>
	[TestModule("dd1657d4-2a66-42a5-a4f8-f3ad9e0c34e5", ModuleType.Recording, 1)]
	public partial class multiCSV_package : ITestModule
	{
		/// <summary>
		/// Holds an instance of the omnius_vf_nl_ranorex_v2.omnius_vf_nl_ranorex_v2Repository repository.
		/// </summary>
		public static omnius_vf_nl_ranorex_v2.omnius_vf_nl_ranorex_v2Repository repo = omnius_vf_nl_ranorex_v2.omnius_vf_nl_ranorex_v2Repository.Instance;

		static multiCSV_package instance = new multiCSV_package();

		/// <summary>
		/// Constructs a new instance.
		/// </summary>
		public multiCSV_package()
		{
			var_package_types = "";
			var_package_subscr = "";
			var_package_devicesubscr = "";
			var_package_addon = "";
		}

		/// <summary>
		/// Gets a static instance of this recording.
		/// </summary>
		public static multiCSV_package Instance
		{
			get { return instance; }
		}

#region Variables

		string _var_package_types;

		/// <summary>
		/// Gets or sets the value of variable var_package_types.
		/// </summary>
		[TestVariable("b6d02d06-a6f8-49b6-9b23-d7c6b67b973a")]
		public string var_package_types
		{
			get { return _var_package_types; }
			set { _var_package_types = value; }
		}

		string _var_package_subscr;

		/// <summary>
		/// Gets or sets the value of variable var_package_subscr.
		/// </summary>
		[TestVariable("f84d2eab-a919-47f2-b6d2-dfd48ae6fc21")]
		public string var_package_subscr
		{
			get { return _var_package_subscr; }
			set { _var_package_subscr = value; }
		}

		string _var_package_devicesubscr;

		/// <summary>
		/// Gets or sets the value of variable var_package_devicesubscr.
		/// </summary>
		[TestVariable("186f35e3-eb5e-4206-8fdb-0fc843383c6e")]
		public string var_package_devicesubscr
		{
			get { return _var_package_devicesubscr; }
			set { _var_package_devicesubscr = value; }
		}

		/// <summary>
		/// Gets or sets the value of variable var_package_addon.
		/// </summary>
		[TestVariable("87b395a0-0df0-41fe-9a94-5f337e90e77c")]
		public string var_package_addon
		{
			get { return repo.var_package_addon; }
			set { repo.var_package_addon = value; }
		}

#endregion

		/// <summary>
		/// Starts the replay of the static recording <see cref="Instance"/>.
		/// </summary>
		[System.CodeDom.Compiler.GeneratedCode("Ranorex", "6.0")]
		public static void Start()
		{
			TestModuleRunner.Run(Instance);
		}

		/// <summary>
		/// Performs the playback of actions in this recording.
		/// </summary>
		/// <remarks>You should not call this method directly, instead pass the module
		/// instance to the <see cref="TestModuleRunner.Run(ITestModule)"/> method
		/// that will in turn invoke this method.</remarks>
		[System.CodeDom.Compiler.GeneratedCode("Ranorex", "6.0")]
		void ITestModule.Run()
		{
			Mouse.DefaultMoveTime = 300;
			Keyboard.DefaultKeyPressTime = 100;
			Delay.SpeedFactor = 1.00;

			Init();

			openCSV();
			Delay.Milliseconds(0);
			
			CSVWriteHeaders();
			Delay.Milliseconds(0);
			
			CSVPrepareData(var_package_types, var_package_subscr, var_package_devicesubscr, var_package_addon);
			Delay.Milliseconds(0);
			
			CSVWriteData();
			Delay.Milliseconds(0);
			
			closeCSV();
			Delay.Milliseconds(0);
			
		}

#region Image Feature Data
#endregion
	}
#pragma warning restore 0436
}
