﻿///////////////////////////////////////////////////////////////////////////////
//
// This file was automatically generated by RANOREX.
// DO NOT MODIFY THIS FILE! It is regenerated by the designer.
// All your modifications will be lost!
// http://www.ranorex.com
//
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Threading;
using WinForms = System.Windows.Forms;

using Ranorex;
using Ranorex.Core;
using Ranorex.Core.Testing;
using Ranorex.Core.Repository;

namespace omnius_vf_nl_ranorex_v2.Recordings.Multi
{
#pragma warning disable 0436 //(CS0436) The type 'type' in 'assembly' conflicts with the imported type 'type2' in 'assembly'. Using the type defined in 'assembly'.
	/// <summary>
	///The multiCSV_porting recording.
	/// </summary>
	[TestModule("5c5112c0-203a-4dea-880b-675c376fe98b", ModuleType.Recording, 1)]
	public partial class multiCSV_porting : ITestModule
	{
		/// <summary>
		/// Holds an instance of the omnius_vf_nl_ranorex_v2.omnius_vf_nl_ranorex_v2Repository repository.
		/// </summary>
		public static omnius_vf_nl_ranorex_v2.omnius_vf_nl_ranorex_v2Repository repo = omnius_vf_nl_ranorex_v2.omnius_vf_nl_ranorex_v2Repository.Instance;

		static multiCSV_porting instance = new multiCSV_porting();

		/// <summary>
		/// Constructs a new instance.
		/// </summary>
		public multiCSV_porting()
		{
			var_porting_type = "";
			var_porting_number_porting_type = "";
			var_porting_check = "";
			var_porting_mobile_number = "";
			var_porting_sim = "";
			var_porting_current_provider = "";
			var_porting_contract = "";
			var_porting_end_date_contract = "";
		}

		/// <summary>
		/// Gets a static instance of this recording.
		/// </summary>
		public static multiCSV_porting Instance
		{
			get { return instance; }
		}

#region Variables

		string _var_porting_type;

		/// <summary>
		/// Gets or sets the value of variable var_porting_type.
		/// </summary>
		[TestVariable("116d63fd-d40d-4df9-a0df-9e91d0d79509")]
		public string var_porting_type
		{
			get { return _var_porting_type; }
			set { _var_porting_type = value; }
		}

		string _var_porting_number_porting_type;

		/// <summary>
		/// Gets or sets the value of variable var_porting_number_porting_type.
		/// </summary>
		[TestVariable("eebf8928-5ded-4d45-9d5e-f078d2b24859")]
		public string var_porting_number_porting_type
		{
			get { return _var_porting_number_porting_type; }
			set { _var_porting_number_porting_type = value; }
		}

		string _var_porting_check;

		/// <summary>
		/// Gets or sets the value of variable var_porting_check.
		/// </summary>
		[TestVariable("c997db73-d7b0-429d-b4f4-0a854db10b6b")]
		public string var_porting_check
		{
			get { return _var_porting_check; }
			set { _var_porting_check = value; }
		}

		string _var_porting_mobile_number;

		/// <summary>
		/// Gets or sets the value of variable var_porting_mobile_number.
		/// </summary>
		[TestVariable("46c5a7c9-be65-4db2-89e8-71674b533047")]
		public string var_porting_mobile_number
		{
			get { return _var_porting_mobile_number; }
			set { _var_porting_mobile_number = value; }
		}

		string _var_porting_sim;

		/// <summary>
		/// Gets or sets the value of variable var_porting_sim.
		/// </summary>
		[TestVariable("6f9ba3bc-bf57-407c-a0e2-51df767b11ce")]
		public string var_porting_sim
		{
			get { return _var_porting_sim; }
			set { _var_porting_sim = value; }
		}

		string _var_porting_current_provider;

		/// <summary>
		/// Gets or sets the value of variable var_porting_current_provider.
		/// </summary>
		[TestVariable("8d7601df-1dbf-4f1f-aeaf-9e53940e749a")]
		public string var_porting_current_provider
		{
			get { return _var_porting_current_provider; }
			set { _var_porting_current_provider = value; }
		}

		string _var_porting_contract;

		/// <summary>
		/// Gets or sets the value of variable var_porting_contract.
		/// </summary>
		[TestVariable("64f767cc-ee23-4c04-8276-5f7557d61665")]
		public string var_porting_contract
		{
			get { return _var_porting_contract; }
			set { _var_porting_contract = value; }
		}

		string _var_porting_end_date_contract;

		/// <summary>
		/// Gets or sets the value of variable var_porting_end_date_contract.
		/// </summary>
		[TestVariable("8cf435c7-d9f7-4780-9793-926c3b63d662")]
		public string var_porting_end_date_contract
		{
			get { return _var_porting_end_date_contract; }
			set { _var_porting_end_date_contract = value; }
		}

#endregion

		/// <summary>
		/// Starts the replay of the static recording <see cref="Instance"/>.
		/// </summary>
		[System.CodeDom.Compiler.GeneratedCode("Ranorex", "6.0")]
		public static void Start()
		{
			TestModuleRunner.Run(Instance);
		}

		/// <summary>
		/// Performs the playback of actions in this recording.
		/// </summary>
		/// <remarks>You should not call this method directly, instead pass the module
		/// instance to the <see cref="TestModuleRunner.Run(ITestModule)"/> method
		/// that will in turn invoke this method.</remarks>
		[System.CodeDom.Compiler.GeneratedCode("Ranorex", "6.0")]
		void ITestModule.Run()
		{
			Mouse.DefaultMoveTime = 300;
			Keyboard.DefaultKeyPressTime = 100;
			Delay.SpeedFactor = 1.00;

			Init();

			openCSV();
			Delay.Milliseconds(0);
			
			CSVWriteHeaders();
			Delay.Milliseconds(0);
			
			CSVPrepareData(var_porting_check, var_porting_number_porting_type, var_porting_type, var_porting_mobile_number, var_porting_sim, var_porting_current_provider, var_porting_contract, var_porting_end_date_contract);
			Delay.Milliseconds(0);
			
			CSVWriteData();
			Delay.Milliseconds(0);
			
			closeCSV();
			Delay.Milliseconds(0);
			
		}

#region Image Feature Data
#endregion
	}
#pragma warning restore 0436
}
