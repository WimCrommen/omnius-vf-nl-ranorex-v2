﻿///////////////////////////////////////////////////////////////////////////////
//
// This file was automatically generated by RANOREX.
// DO NOT MODIFY THIS FILE! It is regenerated by the designer.
// All your modifications will be lost!
// http://www.ranorex.com
//
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Threading;
using WinForms = System.Windows.Forms;

using Ranorex;
using Ranorex.Core;
using Ranorex.Core.Testing;
using Ranorex.Core.Repository;

namespace omnius_vf_nl_ranorex_v2.Recordings.Winkelwagen
{
#pragma warning disable 0436 //(CS0436) The type 'type' in 'assembly' conflicts with the imported type 'type2' in 'assembly'. Using the type defined in 'assembly'.
	/// <summary>
	///The MobilePackageAddon recording.
	/// </summary>
	[TestModule("d065c60c-538a-48e1-9eb8-1683850c2094", ModuleType.Recording, 1)]
	public partial class MobilePackageAddon : ITestModule
	{
		/// <summary>
		/// Holds an instance of the omnius_vf_nl_ranorex_v2.omnius_vf_nl_ranorex_v2Repository repository.
		/// </summary>
		public static omnius_vf_nl_ranorex_v2.omnius_vf_nl_ranorex_v2Repository repo = omnius_vf_nl_ranorex_v2.omnius_vf_nl_ranorex_v2Repository.Instance;

		static MobilePackageAddon instance = new MobilePackageAddon();

		/// <summary>
		/// Constructs a new instance.
		/// </summary>
		public MobilePackageAddon()
		{
			var_package_addon = "1 GB Extra";
		}

		/// <summary>
		/// Gets a static instance of this recording.
		/// </summary>
		public static MobilePackageAddon Instance
		{
			get { return instance; }
		}

#region Variables

		/// <summary>
		/// Gets or sets the value of variable var_package_addon.
		/// </summary>
		[TestVariable("0506e5bf-cd24-4043-a1be-49254ced7d98")]
		public string var_package_addon
		{
			get { return repo.var_package_addon; }
			set { repo.var_package_addon = value; }
		}

#endregion

		/// <summary>
		/// Starts the replay of the static recording <see cref="Instance"/>.
		/// </summary>
		[System.CodeDom.Compiler.GeneratedCode("Ranorex", "6.0")]
		public static void Start()
		{
			TestModuleRunner.Run(Instance);
		}

		/// <summary>
		/// Performs the playback of actions in this recording.
		/// </summary>
		/// <remarks>You should not call this method directly, instead pass the module
		/// instance to the <see cref="TestModuleRunner.Run(ITestModule)"/> method
		/// that will in turn invoke this method.</remarks>
		[System.CodeDom.Compiler.GeneratedCode("Ranorex", "6.0")]
		void ITestModule.Run()
		{
			Mouse.DefaultMoveTime = 300;
			Keyboard.DefaultKeyPressTime = 100;
			Delay.SpeedFactor = 1.00;

			Init();

			Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'Generic.AddonBlock' at Center.", repo.Generic.AddonBlockInfo, new RecordItemIndex(0));
			repo.Generic.AddonBlock.Click();
			Delay.Milliseconds(200);
			
			Report.Log(ReportLevel.Info, "Keyboard", "Key sequence from variable '$var_package_addon' with focus on 'Generic.SearchAddon'.", repo.Generic.SearchAddonInfo, new RecordItemIndex(1));
			repo.Generic.SearchAddon.PressKeys(var_package_addon);
			Delay.Milliseconds(100);
			
			Report.Log(ReportLevel.Info, "Wait", "Waiting 5s for item 'Generic.SpanTag1GBExtra' to exist.", repo.Generic.SpanTag1GBExtraInfo, new ActionTimeout(5000), new RecordItemIndex(2));
			repo.Generic.SpanTag1GBExtraInfo.WaitForExists(5000);
			
			Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'Generic.SpanTag1GBExtra' at Center.", repo.Generic.SpanTag1GBExtraInfo, new RecordItemIndex(3));
			repo.Generic.SpanTag1GBExtra.Click();
			Delay.Milliseconds(200);
			
			Report.Log(ReportLevel.Info, "Validation", "Validating AttributeContains (InnerText>$var_package_addon) on item 'Generic.DivTagWrapperAddon'.", repo.Generic.DivTagWrapperAddonInfo, new RecordItemIndex(4));
			Validate.Attribute(repo.Generic.DivTagWrapperAddonInfo, "InnerText", new Regex(Regex.Escape(var_package_addon)));
			Delay.Milliseconds(0);
			
		}

#region Image Feature Data
#endregion
	}
#pragma warning restore 0436
}
