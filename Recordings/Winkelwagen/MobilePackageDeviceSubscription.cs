﻿///////////////////////////////////////////////////////////////////////////////
//
// This file was automatically generated by RANOREX.
// DO NOT MODIFY THIS FILE! It is regenerated by the designer.
// All your modifications will be lost!
// http://www.ranorex.com
//
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Threading;
using WinForms = System.Windows.Forms;

using Ranorex;
using Ranorex.Core;
using Ranorex.Core.Testing;
using Ranorex.Core.Repository;

namespace omnius_vf_nl_ranorex_v2.Recordings.Winkelwagen
{
#pragma warning disable 0436 //(CS0436) The type 'type' in 'assembly' conflicts with the imported type 'type2' in 'assembly'. Using the type defined in 'assembly'.
	/// <summary>
	///The MobilePackageDeviceSubscription recording.
	/// </summary>
	[TestModule("48e5130f-615e-4d4c-a0af-d4c4cb8687c1", ModuleType.Recording, 1)]
	public partial class MobilePackageDeviceSubscription : ITestModule
	{
		/// <summary>
		/// Holds an instance of the omnius_vf_nl_ranorex_v2.omnius_vf_nl_ranorex_v2Repository repository.
		/// </summary>
		public static omnius_vf_nl_ranorex_v2.omnius_vf_nl_ranorex_v2Repository repo = omnius_vf_nl_ranorex_v2.omnius_vf_nl_ranorex_v2Repository.Instance;

		static MobilePackageDeviceSubscription instance = new MobilePackageDeviceSubscription();

		/// <summary>
		/// Constructs a new instance.
		/// </summary>
		public MobilePackageDeviceSubscription()
		{
			var_package_devicesubscription = "Toestelbetaling Red B 1 j";
		}

		/// <summary>
		/// Gets a static instance of this recording.
		/// </summary>
		public static MobilePackageDeviceSubscription Instance
		{
			get { return instance; }
		}

#region Variables

		/// <summary>
		/// Gets or sets the value of variable var_package_devicesubscription.
		/// </summary>
		[TestVariable("ca9451c9-0263-4a4f-99ef-f1afe4537a38")]
		public string var_package_devicesubscription
		{
			get { return repo.var_package_devicesubscription; }
			set { repo.var_package_devicesubscription = value; }
		}

#endregion

		/// <summary>
		/// Starts the replay of the static recording <see cref="Instance"/>.
		/// </summary>
		[System.CodeDom.Compiler.GeneratedCode("Ranorex", "6.0")]
		public static void Start()
		{
			TestModuleRunner.Run(Instance);
		}

		/// <summary>
		/// Performs the playback of actions in this recording.
		/// </summary>
		/// <remarks>You should not call this method directly, instead pass the module
		/// instance to the <see cref="TestModuleRunner.Run(ITestModule)"/> method
		/// that will in turn invoke this method.</remarks>
		[System.CodeDom.Compiler.GeneratedCode("Ranorex", "6.0")]
		void ITestModule.Run()
		{
			Mouse.DefaultMoveTime = 300;
			Keyboard.DefaultKeyPressTime = 100;
			Delay.SpeedFactor = 1.00;

			Init();

			Report.Log(ReportLevel.Info, "Keyboard", "Key sequence from variable '$var_package_devicesubscription' with focus on 'Generic.SearchDevicesubscription'.", repo.Generic.SearchDevicesubscriptionInfo, new RecordItemIndex(0));
			repo.Generic.SearchDevicesubscription.PressKeys(var_package_devicesubscription);
			Delay.Milliseconds(100);
			
			Report.Log(ReportLevel.Info, "Wait", "Waiting 5s for item 'Generic.ToestelbetalingRedB1Jaar' to exist.", repo.Generic.ToestelbetalingRedB1JaarInfo, new ActionTimeout(5000), new RecordItemIndex(1));
			repo.Generic.ToestelbetalingRedB1JaarInfo.WaitForExists(5000);
			
			Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'Generic.ToestelbetalingRedB1Jaar' at Center.", repo.Generic.ToestelbetalingRedB1JaarInfo, new RecordItemIndex(2));
			repo.Generic.ToestelbetalingRedB1Jaar.Click();
			Delay.Milliseconds(200);
			
			//Report.Log(ReportLevel.Info, "Validation", "Validating AttributeContains (InnerText>$var_package_devicesubscription) on item 'Generic.DivTagToestelbetalingRedB1J'.", repo.Generic.DivTagToestelbetalingRedB1JInfo, new RecordItemIndex(3));
			//Validate.Attribute(repo.Generic.DivTagToestelbetalingRedB1JInfo, "InnerText", new Regex(Regex.Escape(var_package_devicesubscription)));
			//Delay.Milliseconds(0);
			
			Validate_DivTagToestelbetalingRedB1J();
			Delay.Milliseconds(0);
			
		}

#region Image Feature Data
#endregion
	}
#pragma warning restore 0436
}
