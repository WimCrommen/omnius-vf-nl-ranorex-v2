﻿///////////////////////////////////////////////////////////////////////////////
//
// This file was automatically generated by RANOREX.
// DO NOT MODIFY THIS FILE! It is regenerated by the designer.
// All your modifications will be lost!
// http://www.ranorex.com
//
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Threading;
using WinForms = System.Windows.Forms;

using Ranorex;
using Ranorex.Core;
using Ranorex.Core.Testing;
using Ranorex.Core.Repository;

namespace omnius_vf_nl_ranorex_v2.Recordings.Klantgegevens
{
#pragma warning disable 0436 //(CS0436) The type 'type' in 'assembly' conflicts with the imported type 'type2' in 'assembly'. Using the type defined in 'assembly'.
	/// <summary>
	///The Klantgegevens_particulier recording.
	/// </summary>
	[TestModule("2d33f889-1b9d-4759-816e-703a4651f84b", ModuleType.Recording, 1)]
	public partial class Klantgegevens_particulier : ITestModule
	{
		/// <summary>
		/// Holds an instance of the omnius_vf_nl_ranorex_v2.omnius_vf_nl_ranorex_v2Repository repository.
		/// </summary>
		public static omnius_vf_nl_ranorex_v2.omnius_vf_nl_ranorex_v2Repository repo = omnius_vf_nl_ranorex_v2.omnius_vf_nl_ranorex_v2Repository.Instance;

		static Klantgegevens_particulier instance = new Klantgegevens_particulier();

		/// <summary>
		/// Constructs a new instance.
		/// </summary>
		public Klantgegevens_particulier()
		{
			var_cust_first = "";
			var_cust_middle = "";
			var_cust_last = "";
			var_cust_dob = "";
		}

		/// <summary>
		/// Gets a static instance of this recording.
		/// </summary>
		public static Klantgegevens_particulier Instance
		{
			get { return instance; }
		}

#region Variables

		string _var_cust_first;

		/// <summary>
		/// Gets or sets the value of variable var_cust_first.
		/// </summary>
		[TestVariable("13286691-1f44-48c1-8562-e1e2d24aa2e4")]
		public string var_cust_first
		{
			get { return _var_cust_first; }
			set { _var_cust_first = value; }
		}

		string _var_cust_middle;

		/// <summary>
		/// Gets or sets the value of variable var_cust_middle.
		/// </summary>
		[TestVariable("9464fced-efb2-4873-be3d-ae231442fe26")]
		public string var_cust_middle
		{
			get { return _var_cust_middle; }
			set { _var_cust_middle = value; }
		}

		string _var_cust_last;

		/// <summary>
		/// Gets or sets the value of variable var_cust_last.
		/// </summary>
		[TestVariable("78e09ef5-2f92-4904-8398-7af866dec20b")]
		public string var_cust_last
		{
			get { return _var_cust_last; }
			set { _var_cust_last = value; }
		}

		string _var_cust_dob;

		/// <summary>
		/// Gets or sets the value of variable var_cust_dob.
		/// </summary>
		[TestVariable("1daf45de-23fe-47e0-a591-995be0de0c37")]
		public string var_cust_dob
		{
			get { return _var_cust_dob; }
			set { _var_cust_dob = value; }
		}

		/// <summary>
		/// Gets or sets the value of variable var_cust_type.
		/// </summary>
		[TestVariable("6ab9d09c-90a4-44ec-a02a-d2d27a7b0114")]
		public string var_cust_type
		{
			get { return repo.var_cust_type; }
			set { repo.var_cust_type = value; }
		}

		/// <summary>
		/// Gets or sets the value of variable var_cust_gender.
		/// </summary>
		[TestVariable("59309549-878f-4142-9653-388db789f691")]
		public string var_cust_gender
		{
			get { return repo.var_cust_gender; }
			set { repo.var_cust_gender = value; }
		}

#endregion

		/// <summary>
		/// Starts the replay of the static recording <see cref="Instance"/>.
		/// </summary>
		[System.CodeDom.Compiler.GeneratedCode("Ranorex", "6.0")]
		public static void Start()
		{
			TestModuleRunner.Run(Instance);
		}

		/// <summary>
		/// Performs the playback of actions in this recording.
		/// </summary>
		/// <remarks>You should not call this method directly, instead pass the module
		/// instance to the <see cref="TestModuleRunner.Run(ITestModule)"/> method
		/// that will in turn invoke this method.</remarks>
		[System.CodeDom.Compiler.GeneratedCode("Ranorex", "6.0")]
		void ITestModule.Run()
		{
			Mouse.DefaultMoveTime = 300;
			Keyboard.DefaultKeyPressTime = 100;
			Delay.SpeedFactor = 1.00;

			Init();

			Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'Generic.CustomerType' at Center.", repo.Generic.CustomerTypeInfo, new RecordItemIndex(0));
			repo.Generic.CustomerType.Click();
			Delay.Milliseconds(200);
			
			Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'Generic.CustomerGender' at Center.", repo.Generic.CustomerGenderInfo, new RecordItemIndex(1));
			repo.Generic.CustomerGender.Click();
			Delay.Milliseconds(200);
			
			Report.Log(ReportLevel.Info, "Keyboard", "Key sequence from variable '$var_cust_first' with focus on 'Generic.CustomerFirstname'.", repo.Generic.CustomerFirstnameInfo, new RecordItemIndex(2));
			repo.Generic.CustomerFirstname.PressKeys(var_cust_first);
			Delay.Milliseconds(100);
			
			Report.Log(ReportLevel.Info, "Keyboard", "Key sequence from variable '$var_cust_middle' with focus on 'Generic.CustomerMiddlename'.", repo.Generic.CustomerMiddlenameInfo, new RecordItemIndex(3));
			repo.Generic.CustomerMiddlename.PressKeys(var_cust_middle);
			Delay.Milliseconds(100);
			
			Report.Log(ReportLevel.Info, "Keyboard", "Key sequence from variable '$var_cust_last' with focus on 'Generic.CustomerLastname'.", repo.Generic.CustomerLastnameInfo, new RecordItemIndex(4));
			repo.Generic.CustomerLastname.PressKeys(var_cust_last);
			Delay.Milliseconds(100);
			
			Report.Log(ReportLevel.Info, "Keyboard", "Key sequence from variable '$var_cust_dob' with focus on 'Generic.CustomerDob'.", repo.Generic.CustomerDobInfo, new RecordItemIndex(5));
			repo.Generic.CustomerDob.PressKeys(var_cust_dob);
			Delay.Milliseconds(100);
			
		}

#region Image Feature Data
#endregion
	}
#pragma warning restore 0436
}
