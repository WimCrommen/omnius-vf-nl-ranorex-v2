﻿///////////////////////////////////////////////////////////////////////////////
//
// This file was automatically generated by RANOREX.
// DO NOT MODIFY THIS FILE! It is regenerated by the designer.
// All your modifications will be lost!
// http://www.ranorex.com
//
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Threading;
using WinForms = System.Windows.Forms;

using Ranorex;
using Ranorex.Core;
using Ranorex.Core.Testing;
using Ranorex.Core.Repository;

namespace omnius_vf_nl_ranorex_v2.Recordings.Klantgegevens
{
#pragma warning disable 0436 //(CS0436) The type 'type' in 'assembly' conflicts with the imported type 'type2' in 'assembly'. Using the type defined in 'assembly'.
	/// <summary>
	///The ZoekKlantOrder recording.
	/// </summary>
	[TestModule("b66aa7e2-bd4e-42d3-9b5f-df5264fda0d7", ModuleType.Recording, 1)]
	public partial class ZoekKlantOrder : ITestModule
	{
		/// <summary>
		/// Holds an instance of the omnius_vf_nl_ranorex_v2.omnius_vf_nl_ranorex_v2Repository repository.
		/// </summary>
		public static omnius_vf_nl_ranorex_v2.omnius_vf_nl_ranorex_v2Repository repo = omnius_vf_nl_ranorex_v2.omnius_vf_nl_ranorex_v2Repository.Instance;

		static ZoekKlantOrder instance = new ZoekKlantOrder();

		/// <summary>
		/// Constructs a new instance.
		/// </summary>
		public ZoekKlantOrder()
		{
			var_order_id = "";
		}

		/// <summary>
		/// Gets a static instance of this recording.
		/// </summary>
		public static ZoekKlantOrder Instance
		{
			get { return instance; }
		}

#region Variables

		string _var_order_id;

		/// <summary>
		/// Gets or sets the value of variable var_order_id.
		/// </summary>
		[TestVariable("d041e611-b33a-48b2-a14d-b40f45b109f9")]
		public string var_order_id
		{
			get { return _var_order_id; }
			set { _var_order_id = value; }
		}

#endregion

		/// <summary>
		/// Starts the replay of the static recording <see cref="Instance"/>.
		/// </summary>
		[System.CodeDom.Compiler.GeneratedCode("Ranorex", "6.0")]
		public static void Start()
		{
			TestModuleRunner.Run(Instance);
		}

		/// <summary>
		/// Performs the playback of actions in this recording.
		/// </summary>
		/// <remarks>You should not call this method directly, instead pass the module
		/// instance to the <see cref="TestModuleRunner.Run(ITestModule)"/> method
		/// that will in turn invoke this method.</remarks>
		[System.CodeDom.Compiler.GeneratedCode("Ranorex", "6.0")]
		void ITestModule.Run()
		{
			Mouse.DefaultMoveTime = 300;
			Keyboard.DefaultKeyPressTime = 100;
			Delay.SpeedFactor = 1.00;

			Init();

			Report.Log(ReportLevel.Info, "Keyboard", "Key sequence from variable '$var_order_id' with focus on 'Generic.CustomerSearchOrderNumber'.", repo.Generic.CustomerSearchOrderNumberInfo, new RecordItemIndex(0));
			repo.Generic.CustomerSearchOrderNumber.PressKeys(var_order_id);
			Delay.Milliseconds(100);
			
			Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'Generic.CustomerSearchButton' at Center.", repo.Generic.CustomerSearchButtonInfo, new RecordItemIndex(1));
			repo.Generic.CustomerSearchButton.Click();
			Delay.Milliseconds(200);
			
			Report.Log(ReportLevel.Info, "Wait", "Waiting 2m for item 'Generic.OrderInfoLeft' to exist.", repo.Generic.OrderInfoLeftInfo, new ActionTimeout(120000), new RecordItemIndex(2));
			repo.Generic.OrderInfoLeftInfo.WaitForExists(120000);
			
			Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'Generic.OrderInfoLeft' at Center.", repo.Generic.OrderInfoLeftInfo, new RecordItemIndex(3));
			repo.Generic.OrderInfoLeft.Click();
			Delay.Milliseconds(200);
			
		}

#region Image Feature Data
#endregion
	}
#pragma warning restore 0436
}
