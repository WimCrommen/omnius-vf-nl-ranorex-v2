﻿///////////////////////////////////////////////////////////////////////////////
//
// This file was automatically generated by RANOREX.
// Your custom recording code should go in this file.
// The designer will only add methods to this file, so your custom code won't be overwritten.
// http://www.ranorex.com
// 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Threading;
using WinForms = System.Windows.Forms;

using Ranorex;
using Ranorex.Core;
using Ranorex.Core.Testing;

namespace omnius_vf_nl_ranorex_v2.Recordings.Klantgegevens
{
	public partial class Klantgegevens_zakelijk_id
	{
		/// <summary>
		/// This method gets called right after the recording has been started.
		/// It can be used to execute recording specific initialization code.
		/// </summary>
		private void Init()
		{
			// Your recording specific initialization code goes here.
		}

		public void Set_Value_CustomerContractantIdType()
		{
			Report.Log(ReportLevel.Info, "Set Value", "Setting attribute InnerText to '$var_cust_id' on item 'Generic.CustomerContractantIdType'.", repo.Generic.CustomerContractantIdTypeInfo);
			
			IList<OptionTag> optionlist = repo.Generic.CustomerContractantIdType.FindChildren<OptionTag>();
			foreach (OptionTag optionitem in optionlist)
			{
				if (optionitem.Label != null)
				{
					if (optionitem.Label.ToLower() == var_cust_id.ToLower())
					{
						repo.Generic.CustomerContractantIdType.Element.SetAttributeValue("TagValue", optionitem.Value);
						break;
					}
				}
			}
		}

		public void Set_Value_CustomerContractantIssuingCountry()
		{
			Report.Log(ReportLevel.Info, "Set Value", "Setting attribute InnerText to '$var_cust_id_country' on item 'Generic.CustomerContractantIssuingCountry'.", repo.Generic.CustomerContractantIssuingCountryInfo);
			
			IList<OptionTag> optionlist = repo.Generic.CustomerContractantIssuingCountry.FindChildren<OptionTag>();
			foreach (OptionTag optionitem in optionlist)
			{
				if (optionitem.Label != null)
				{
					if (optionitem.Label.ToLower() == var_cust_id_country.ToLower())
					{
						repo.Generic.CustomerContractantIssuingCountry.Element.SetAttributeValue("TagValue", optionitem.Value);
						break;
					}
				}
			}
		}

	}
}