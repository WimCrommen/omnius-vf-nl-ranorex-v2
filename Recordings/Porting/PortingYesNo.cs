﻿///////////////////////////////////////////////////////////////////////////////
//
// This file was automatically generated by RANOREX.
// DO NOT MODIFY THIS FILE! It is regenerated by the designer.
// All your modifications will be lost!
// http://www.ranorex.com
//
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Threading;
using WinForms = System.Windows.Forms;

using Ranorex;
using Ranorex.Core;
using Ranorex.Core.Testing;
using Ranorex.Core.Repository;

namespace omnius_vf_nl_ranorex_v2.Recordings.Porting
{
#pragma warning disable 0436 //(CS0436) The type 'type' in 'assembly' conflicts with the imported type 'type2' in 'assembly'. Using the type defined in 'assembly'.
	/// <summary>
	///The PortingYesNo recording.
	/// </summary>
	[TestModule("19bf6ac3-f635-4cf0-9387-f8c3b9e25e45", ModuleType.Recording, 1)]
	public partial class PortingYesNo : ITestModule
	{
		/// <summary>
		/// Holds an instance of the omnius_vf_nl_ranorex_v2.omnius_vf_nl_ranorex_v2Repository repository.
		/// </summary>
		public static omnius_vf_nl_ranorex_v2.omnius_vf_nl_ranorex_v2Repository repo = omnius_vf_nl_ranorex_v2.omnius_vf_nl_ranorex_v2Repository.Instance;

		static PortingYesNo instance = new PortingYesNo();

		/// <summary>
		/// Constructs a new instance.
		/// </summary>
		public PortingYesNo()
		{
			var_porting_check = "";
			var_porting_tc_no = "";
			var_porting_tc_yes = "";
		}

		/// <summary>
		/// Gets a static instance of this recording.
		/// </summary>
		public static PortingYesNo Instance
		{
			get { return instance; }
		}

#region Variables

		string _var_porting_check;

		/// <summary>
		/// Gets or sets the value of variable var_porting_check.
		/// </summary>
		[TestVariable("25f7cb3f-b210-4adf-95e0-05d78d7f7b08")]
		public string var_porting_check
		{
			get { return _var_porting_check; }
			set { _var_porting_check = value; }
		}

		string _var_porting_tc_no;

		/// <summary>
		/// Gets or sets the value of variable var_porting_tc_no.
		/// </summary>
		[TestVariable("34ddb5b1-30d5-457c-accc-c486e674f972")]
		public string var_porting_tc_no
		{
			get { return _var_porting_tc_no; }
			set { _var_porting_tc_no = value; }
		}

		string _var_porting_tc_yes;

		/// <summary>
		/// Gets or sets the value of variable var_porting_tc_yes.
		/// </summary>
		[TestVariable("1bd2ddf4-1c1c-439f-8211-42507cdc2afc")]
		public string var_porting_tc_yes
		{
			get { return _var_porting_tc_yes; }
			set { _var_porting_tc_yes = value; }
		}

#endregion

		/// <summary>
		/// Starts the replay of the static recording <see cref="Instance"/>.
		/// </summary>
		[System.CodeDom.Compiler.GeneratedCode("Ranorex", "6.0")]
		public static void Start()
		{
			TestModuleRunner.Run(Instance);
		}

		/// <summary>
		/// Performs the playback of actions in this recording.
		/// </summary>
		/// <remarks>You should not call this method directly, instead pass the module
		/// instance to the <see cref="TestModuleRunner.Run(ITestModule)"/> method
		/// that will in turn invoke this method.</remarks>
		[System.CodeDom.Compiler.GeneratedCode("Ranorex", "6.0")]
		void ITestModule.Run()
		{
			Mouse.DefaultMoveTime = 300;
			Keyboard.DefaultKeyPressTime = 100;
			Delay.SpeedFactor = 1.00;

			Init();

			PortingYesNoFunc(var_porting_check, var_porting_tc_no, var_porting_tc_yes);
			Delay.Milliseconds(0);
			
		}

#region Image Feature Data
#endregion
	}
#pragma warning restore 0436
}
